(function (Drupal, once) {
  Drupal.behaviors.imageFadingCarousel = {
    attach: function (context, settings) {
      const carousels = once('image-fading-carousel', '.image-fading-carousel', context);
      carousels.forEach(carousel => {
        let carouselName = carousel.getAttribute('data-carousel-name');
        const config = getCarouselConfig(carouselName, settings);
        if (!config) {
          return null;
        }

        // Initialize img elements with images.
        initializeCarousel(carousel, carouselName, config);

        // Change url on random image per defined change interval.
        if (config.availableImagesCount > config.displayableImagesCount) {
          changeRandomImage(carouselName, config, true);
        }
      });

      /**
       * Prepares the configuration object for a carousel.
       *
       * @param {string} carouselName - The unique name of the carousel.
       * @param {object} settings - The general drupal settings object.
       *
       * @return {object|null} The configuration object for the carousel, or null if the configuration is not available or not valid.
       */
      function getCarouselConfig(carouselName, settings) {
        // Skip carousel displaying when no configuration available.
        if (
          !settings.image_fading_carousel[carouselName]
          || !(settings.image_fading_carousel[carouselName].hasOwnProperty('change_interval'))
          || !(settings.image_fading_carousel[carouselName].hasOwnProperty('max_images_to_display'))
          || !(settings.image_fading_carousel[carouselName].hasOwnProperty('image_list_urls'))
          || !(settings.image_fading_carousel[carouselName].image_list_urls instanceof Array)
          || settings.image_fading_carousel[carouselName].image_list_urls.length === 0
        ) {
          return null;
        }

        let config = {};
        let changeIntervalSeconds = 5;
        const changeIntervalSetting = Number(settings.image_fading_carousel[carouselName].change_interval);
        if (Number.isInteger(changeIntervalSetting)) {
          changeIntervalSeconds = changeIntervalSetting;
        }
        config.changeInterval = changeIntervalSeconds * 1000;

        let maxImagesToDisplay = 1;
        const maxImagesToDisplaySetting = Number(settings.image_fading_carousel[carouselName].max_images_to_display);
        if (Number.isInteger(maxImagesToDisplaySetting)) {
          maxImagesToDisplay = maxImagesToDisplaySetting;
        }
        config.maxImagesToDisplay = maxImagesToDisplay;
        config.imageListUrls = settings.image_fading_carousel[carouselName].image_list_urls;
        config.availableImagesCount = config.imageListUrls.length;
        config.displayableImagesCount = Math.min(config.maxImagesToDisplay, config.imageListUrls.length);

        return config;
      }

      /**
       * Initializes a carousel with images.
       *
       * @param {HTMLElement} carousel - The carousel's element.
       * @param {string} carouselName - The unique name of the image carousel.
       * @param {object} config - The configuration object for the image carousel.
       */
      function initializeCarousel(carousel, carouselName, config) {
        // Initialize visible image urls with random images.
        let allUrlsKeys = [...config.imageListUrls.keys()];
        allUrlsKeys = allUrlsKeys.sort(() => Math.random() - 0.5);
        let visibleImageUrls = allUrlsKeys.slice(0, config.displayableImagesCount);

        // Initialize needed structure to hold images.
        const ul = document.createElement('ul');
        visibleImageUrls.forEach(urlKey => {
          const li = document.createElement('li');
          const img = document.createElement('img');
          img.src = config.imageListUrls[urlKey];
          li.appendChild(img);
          ul.appendChild(li);
        });
        carousel.appendChild(ul);

        window.imageFadingCarousel = window.imageFadingCarousel || {};
        window.imageFadingCarousel[carouselName] = {
          visibleImageUrls: visibleImageUrls,
          lastUpdatedImgElementKey: null,
        };
      }

      /**
       * Changes the src of a randomly chosen image element.
       *
       * @param {string} carouselName - The unique name of the image carousel.
       * @param {object} config - The configuration object for the image carousel.
       * @param {boolean} [initPhase=false] - Whether the method is called during the initialization phase.
       */
      function changeRandomImage(carouselName, config, initPhase = false) {
        if (!initPhase) {
          let carouselInfo = window.imageFadingCarousel[carouselName];
          const imgElements = [...document.querySelectorAll(`.image-fading-carousel[data-carousel-name="${carouselName}"] ul li img`)];

          // Use random not-visible image url.
          const allUrlsKeys = [...config.imageListUrls.keys()];
          const notVisibleUrlsKeys = allUrlsKeys.filter(key => !carouselInfo.visibleImageUrls.includes(key));
          const randomIndex = Math.floor(Math.random() * notVisibleUrlsKeys.length);
          const randomUrlKey = notVisibleUrlsKeys[randomIndex];

          // Default to 0 index in case when only one img element displayed.
          let randomImgElementKey = 0;

          // Use random previously not updated img element.
          const allImgElementKeys = [...imgElements.keys()];
          if (allImgElementKeys.length > 1) {
            const updatableImgElementKeys = allImgElementKeys.filter(key => key !== carouselInfo.lastUpdatedImgElementKey);
            const randomUpdatableIndex = Math.floor(Math.random() * updatableImgElementKeys.length);
            randomImgElementKey = updatableImgElementKeys[randomUpdatableIndex];
          }

          // Update `visibleImageUrls` with the new image key.
          carouselInfo.visibleImageUrls[randomImgElementKey] = randomUrlKey;

          // Preload the image to get rid of blinking while loading.
          const hiddenImg = document.createElement('img');
          hiddenImg.style.display = 'none';
          document.body.appendChild(hiddenImg);
          hiddenImg.onload = function() {
            // After it is loaded, use the image for the chosen image element.
            const randomImgElement = imgElements[randomImgElementKey];
            randomImgElement.src = hiddenImg.src;
            randomImgElement.animate([{ opacity: 0 }, { opacity: 1 }], {
              duration: 2000,
              fill: "forwards"
            });

            // Remove the hidden image from document.
            hiddenImg.remove();

            // Schedule a random image change after the image has loaded.
            setTimeout(() => changeRandomImage(carouselName, config), config.changeInterval);
          };
          hiddenImg.src = config.imageListUrls[randomUrlKey];
          carouselInfo.lastUpdatedImgElementKey = randomImgElementKey;
          window.imageFadingCarousel[carouselName] = carouselInfo;
        }
        else {
          // Schedule a random image change after initialization.
          setTimeout(() => changeRandomImage(carouselName, config), config.changeInterval);
        }
      }
    }
  };
})(Drupal, once);
