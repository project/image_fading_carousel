<?php

namespace Drupal\image_fading_carousel\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Crypt;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\image\Plugin\Field\FieldFormatter\ImageUrlFormatter;

/**
 * Plugin implementation of the 'image_fading_carousel' formatter.
 *
 * @FieldFormatter(
 *   id = "image_fading_carousel",
 *   label = @Translation("Image Fading Carousel"),
 *   field_types = {
 *     "image"
 *   }
 * )
 */
class ImageFadingCarouselFormatter extends ImageUrlFormatter {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $default_settings = parent::defaultSettings();
    $default_settings['change_interval'] = 5;
    $default_settings['max_images_to_display'] = 3;
    return $default_settings;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::settingsForm($form, $form_state);

    $element['change_interval'] = [
      '#title' => $this->t('Change Interval'),
      '#type' => 'number',
      '#min' => 1,
      '#default_value' => $this->getSetting('change_interval'),
      '#description' => $this->t('The duration, in seconds, after which a random image in the carousel will be replaced with another one.'),
    ];
    $element['max_images_to_display'] = [
      '#title' => $this->t('Max images to display'),
      '#type' => 'number',
      '#min' => 1,
      '#default_value' => $this->getSetting('max_images_to_display'),
      '#description' => $this->t('The maximum number of images that will be displayed at once in the image carousel.'),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary[] = $this->t('Change Interval: @seconds second(s)', ['@seconds' => $this->getSetting('change_interval')]);
    $summary[] = $this->t('Max images to display: @count', ['@count' => $this->getSetting('max_images_to_display')]);

    return array_merge($summary, parent::settingsSummary());
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    /** @var \Drupal\Core\Field\EntityReferenceFieldItemListInterface $items */
    if (empty($images = $this->getEntitiesToView($items, $langcode))) {
      // Early opt-out if the field is empty.
      return $elements;
    }

    $entity = $items->getEntity();
    $id_parts = [
      $entity->getEntityType()->id(),
      $entity->id(),
      $items->getFieldDefinition()->getName(),
    ];
    $carousel_id = Crypt::hashBase64(implode('--', $id_parts));

    /** @var \Drupal\image\ImageStyleInterface $image_style */
    $image_style = $this->imageStyleStorage->load($this->getSetting('image_style'));
    /** @var \Drupal\Core\File\FileUrlGeneratorInterface $file_url_generator */
    $file_url_generator = \Drupal::service('file_url_generator');
    $image_list_urls = [];
    $cacheability = CacheableMetadata::createFromObject($entity);
    /** @var \Drupal\file\FileInterface[] $images */
    foreach ($images as $delta => $image) {
      $image_uri = $image->getFileUri();
      $url = $image_style ? $file_url_generator->transformRelative($image_style->buildUrl($image_uri)) : $file_url_generator->generateString($image_uri);
      $image_list_urls[] = $url;

      $cacheability->addCacheableDependency(CacheableMetadata::createFromObject($image));
      if ($image_style) {
        $cacheability->addCacheableDependency(CacheableMetadata::createFromObject($image_style));
      }
    }

    $carousel_config = [
      'image_list_urls' => $image_list_urls,
      'change_interval' => $this->getSetting('change_interval'),
      'max_images_to_display' => $this->getSetting('max_images_to_display'),
    ];
    $elements[0] = [
      '#type' => 'container',
      '#attached' => [
        'library' => ['image_fading_carousel/image-fading-carousel'],
        'drupalSettings' => [
          'image_fading_carousel' => [
            $carousel_id => $carousel_config,
          ],
        ],
      ],
      '#attributes' => [
        'class' => ['image-fading-carousel'],
        'data-carousel-name' => $carousel_id,
      ],
    ];
    $cacheability->applyTo($elements[0]);

    return $elements;
  }

}
